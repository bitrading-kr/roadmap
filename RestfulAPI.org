#+TITLE: Restful API

* Order Server & Runner Server

** v1

*** orders
**** GET /v1/orders

 모든 오더 가져오기

 - Headers : authtoken
 - Params: none

**** GET /v1/orders/:id

 특정 오더 가져오기

 - Headers : authtoken
 - Params : none

**** POST /v1/orders/:id

오더 추가하기

**** PUT /v1/orders/:id

오더 업데이트 하기

**** DELETE /v1/orders/:id

오더 삭제하기

*** tradings
**** GET /v1/tradings

 모든 오더 가져오기

 - Headers : authtoken
 - Params: none

**** GET /v1/tradings/:id

 특정 오더 가져오기

 - Headers : authtoken
 - Params : none

*** commands

**** PUT /v1/commands/orders

- active : 오더 활성화
- deacitve : 오더 비활성화

**** PUT /v1/commands/tradings

- cancel : 트레이딩 취소(주문 취소)
