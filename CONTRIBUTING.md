# Contributing

## Issue workflow ##

### 개요 ###
- 종류 : ~API, ~Cluster, ~Apps, ~VM, ~UI, ~Frontend, ~Middleware, ~Backend, ~DB, ~Feature
- 분류 : ~Upbit, ~GCP, ~Ticker, ~Service, ~Broker, ~User, ~Web, ~Android, ~iOS, ~Component, ~Page, ~Layout, ~Product, ~Billing, ~Trading, ~System, ~Design, ~Docker, ~Service
- 상태 : ~Idea, ~"To Do", ~"Doing", ~Test
- 내용 : ~"Implement", ~"Bug"
- 중요도 : ~P1, ~P2, ~P3, ~P4

### 종류 ###

- ~API : API 관련
- ~Cluster : 클러스터 구성
- ~Apps : 앱 구현 관련
- ~VM : 가상화 머신
- ~Frontend : 프론트엔드
- ~Middleware : 미들웨어
- ~Backend : 백엔드
- ~DB : 데이터베이스 관련
- ~UI : 인터페이스 관련
- ~Feature : 기능 관련

### 분류 ###

#### ~API ###
- ~Upbit

#### ~Cluster ####
- ~GCP
- ~Ticker
- ~Service
- ~Broker
- ~User

#### ~Apps ####
- ~Web
- ~Android
- ~iOS

#### ~VM ####
- ~Docker

#### ~UI ####
- ~Design

#### ~Frontend ####
- ~Component
- ~Page
- ~Layout

#### ~Middleware ####

#### ~Backend ####

#### ~DB ####

#### ~Feature ####
- ~Product : 상품 관련
- ~Billing : 결제 관련
- ~Trading : 매매 관련
- ~System : 시스템 관련
- ~Service : 서비스 관련


### 상태 ###

- ~Idea : 아이디어
- ~"To Do" : 해야 할 것
- ~"Doing" : 현재 진행중
- ~Test : 테스트 진행

### 내용 ###

- ~Implement : 구현
- ~Bug : 버그

### 중요도 ###

- ~P1 : 최상 순위 중요도
- ~P2 : 두번째 중요도 
- ~P3 : 세번째 중요도
- ~P4 : 네번째 중요도
