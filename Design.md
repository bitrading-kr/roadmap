# Design

## DB 스키마

### 관계도

![DB-relation](./images/db-relation.png)

### 생성순서

1. group_membership
2. user
3. license
4. user_membership
5. exchange
6. market
7. coin
8. user_api
9. exchange_market
10. exchange_coin
11. order
12. trading
13. entry_strategy
14. target_strategy
15. trailing_stop_strategy

#### user
5. user_api

#### exchange
1. market
2. coin
3. exchange
4. exchange_market
5. exchange_coin

#### order
1. order
2. entry_strategy
3. target_strategy
4. trailling_stop_strategy
5. trading

### user (standalone)

| ID(PK) | login_id     | login_pw     |
|--------|--------------|--------------|
| int    | varchar(100) | varchar(255) |

- status
  - 0 : 일반
  - 1 : 휴면
  - 2 : 탈퇴

### user_membership (dep. user)

| UID(FK) | email        | registered | status  |
|---------|--------------|------------|---------|
| int     | varchar(100) | datetime   | tinyint |

- UID
  - user/ID

### user_license (dep. user)

| UID(FK) | LicenseID(FK) |
|---------|---------------|
| int     | int        |

### user_api (dep. user)

| UID(FK) | ExchangeID(FK) | access_key   | secret_key   |
|---------|----------------|--------------|--------------|
| int     | int            | varchar(255) | varchar(255) |

- UID
  - user/ID
- exchange : 거래소
  - 0 : 업비트

### group (standalone)

| ID(PK) | description  |
|---------|--------------|
| int     | varchar(255) |

### user_group (dep. user, group)

| UID(FK) | GID(FK)      |
|---------|--------------|
| int     | int |

### license (standalone)

| ID(PK) | license_key  | type | expired_date |
|--------|--------------|------|--------------|
| int    | varchar(255) | int  | datetime     |

- license : 라이센스
- type 
  - 0 : 사용자 구매
  - 1 : 트레이더 프로모션
- expired_date : 만료일

### market (standalone)

| ID(PK) | name         | symbol      |
|--------|--------------|-------------|
| int    | varchar(255) | varchar(20) |

### coin (standalone)

| ID(PK) | name         | symbol      |
|--------|--------------|-------------|
| int    | varchar(255) | varchar(20) |

### exchange (standalone)

| ID(PK) | name         | symbol      |
|--------|--------------|-------------|
| int    | varchar(255) | varchar(20) |

### exchange_market (dep. exchange, market)

| exchangeID(FK) | MarketID(FK) |
|----------------|--------------|
| int            | int          |

### exchange_coin (dep. exchange, coin)

| exchangeID(FK) | CoinID(FK) |
|----------------|------------|
| int            | int        |

### order (dep. user, exchange, market, coin)

| ID(PK) | UID(FK) | UAPI(FK) | SourceID | title        | status  | registered | ExchangeID(FK) | MarketID(FK) | CoinID(FK) |
|--------|---------|----------|----------|--------------|---------|------------|----------------|--------------|------------|
| int    | int     | int      | int      | varchar(100) | boolean | datetime   | int            | int          | int        |

- ID
  - 1부터 시작
- UID
  - user/ID
- UAPI
  - user api가 설정되었을 경우 해당 API 로만 오더를 사용하고, NULL 로 설정된 경우 시스템에서 자동으로 배정한다.
- sourceID 
  - 0 : 신규
  - 그외 : 해당 ID 를 기반
- status
  - 0 : off
  - 1 : on
- exchangeID: 거래소 ID
- market : 사용할 통화(KRW/BTC/ETH/USDT)
- coin : 구매할 코인(BTC/ETH/ETC/ADA/...)

### entry_strategy (dep. order, trading)

| OrderID(FK) | price | quantity | TradingID(FK) |
|-------------|-------|----------|---------------|
| int         | float | int      | int           |

- OrderID : order/ID
- price : 매수 가격
- quantity : 매수량

### target_strategy (dep. order, trading)

| OrderID(FK) | price | quantity | TradingID(FK) |
|-------------|-------|----------|---------------|
| int         | float | int      | int           |

- OrderID : order/ID
- price : 타겟 포인트(매도 가격)
- quantity : 매도량

### trailing_stop_strategy (dep. order, trading)

| OrderID(FK) | stop_limit | reduction | TradingID(FK) |
|-------------|------------|-----------|---------------|
| int         | float      | float     | int           |

- OrderID : order/ID
- stop_limit : 손절매 트리거 시작
- reduction : 감소량(%)

### trading (dep. order, uuid)

| ID(PK) | OrderID(FK) | uuid(FK) |
|--------|-------------|----------|
| int    | int         | int      |

- OrderID : order/ID
- uuid : 거래소 UUID
- type : 타입
  - 0 : 매도
  - 1 : 매수
- status : 상태
  - 0 : wait
  - 1 : done
  - 2 : cancel

### uuid (standalone)

| ID(PK) | uuid    | side    | ord_type | price | avg_price | state   | market  | created_at | volume | remaining_volume | reserved_fee | remaining_fee | paid_fee | locked | executed_volume | trade_count |
|--------|---------|---------|----------|-------|-----------|---------|---------|------------|--------|------------------|--------------|---------------|----------|--------|-----------------|-------------|
| int    | varchar | varchar | varchar  | int   | int       | varchar | varchar | varchar    | int    | int              | int          | int           | int      | int    | int             | int         |

## 데이터 구조

### userInfo
```js
{
  "UID": "number",
  "GID": "number",
  "loginId": "string",
  "loginPw": "string",
  "email": "string",
  "registered": "string",
  "status": "number",
  "license": {
    "type": "number",
    "expiredDate": "string"
  },
  "api": [
    {
      "exchangeId": "number",
      "accessKey": "string",
      "secretKey": "string"
    }
  ]
}
```

### orderList
{
  "UID": "number",
  "orderList": [{
    "ID": "number",
    "sourceID": "number",
    "title": "string",
    "status": "number",
    "registered": "string",
    "exchangeID": "number",
    "marketID: "number",
    "coinID": "number"
  }]
}

### orderDetail
