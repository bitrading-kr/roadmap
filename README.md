# Roadmap

프로젝트 T 로드맵

## 프로젝트 구성 ##

- [bitrading-kr](https://gitlab.com/bitrading-kr/bitrading-kr) : 메인 프로젝트로 frontend/middleware/backend 가 구성되어 있습니다.

### 모듈 ###
내부 사용 모듈

- [bitrading-module-manager](https://gitlab.com/bitrading-kr/modules/bitrading-module-manager) : 비트레이딩 모듈 매니저
- [asking-price](https://gitlab.com/bitrading-kr/modules/asking-price) : 거래소별 호가 자동 설정
- [resolve-error](https://gitlab.com/bitrading-kr/modules/resolve-error) : 에러를 파싱하고 template로 설정된 적절한 에러를 보내줍니다. [bitrading-error-template](https://gitlab.com/bitrading-kr/data/bitrading-error-template) 에서 정의된 에러를 사용합니다.
- [resolve-validator](https://gitlab.com/bitrading-kr/modules/resolve-validator) : 입력 데이터를 검증하기 위한 모듈입니다. [bitrading-validator-template](https://gitlab.com/bitrading-kr/data/bitrading-validator-template) 에서 정의된 검증 데이터를 사용합니다.
- [assert-env](https://gitlab.com/bitrading-kr/modules/assert-env) : 새로 정의한 타입을 체크하고 맞지 않는 경우 에러를 던집니다.
- [api/bitrading](https://gitlab.com/bitrading-kr/api/bitrading) : 비트레이딩 DB와 통신하기 위한 백엔드 API 입니다.
- [api/upbit](https://gitlab.com/bitrading-kr/api/upbit) : 업비트 서버와 통신하기 위해 구현된 모듈입니다.

### 마이크로 서비스 ###
- [auth](https://gitlab.com/bitrading-kr/services/auth) : 비트레이딩 인증 서비스입니다. 사용자 회원가입, 로그인, 로그인 인증 등을 수행합니다.
- [worker](https://gitlab.com/bitrading-kr/services/worker) : 비트레이딩 워커 서비스입니다. 실제 내부적으로 거래를 하는 서비스입니다.
  - [order-manager](https://gitlab.com/bitrading-kr/worker/order-manager) : worker에서 주문을 관리하기 위한 모듈입니다.
  - [executor](https://gitlab.com/bitrading-kr/worker/executor) : 주문을 실행하는 모듈로, 실제 사용자별로 주문된 정보를 위해 서버를 할당하여 업비트 서버와 통신하며 주문이 로직에 맞게 실행되는 모듈입니다.
- [order](https://gitlab.com/bitrading-kr/services/order) : 비트레이딩 주문관리 서비스입니다. 사용자 주문을 받고 수정, 삭제등의 기능을 수행하고 worker와 통신합니다.

## 문서 ##
- [Design.md](./Design.md)
- [DataStructure.md](./DataStructure.md)
- [RestfulAPI.org](./RestfulAPI.org)

## 클러스터 구성 ##

### Ticker : 거래소 시장가 클러스터 ###
시장가를 초당 30번 이상 가져오며 메모리에 저장하고 사용자 클러스터에게 전달해주는 클러스터

| 노드명         | CPU | RAM | 특징              |
|----------------|-----|-----|-------------------|
| Ticker API - 1 |   1 |   1 | 빠른속도/제한된 요청 수 |
| Ticker API - 2 |   1 |   1 | 빠른속도/제한된 요청 수 |
| Chrome Driver  |   4 |   4 | 실시간반영/많은 자원 수 |

### Serivce : 서비스 클러스터 ###
서비스 클러스터(웹, 앱)

### Broker : 브로커 클러스터 ###
사용자 설정을 각 VM에 할당하고 모니터링 하는 클러스터

### User : 사용자 클러스터 ###
실시간 사용자 API를 통해 거래소에 트레이드 요청을 하는 클러스터

## 동적 클라우드 구성 ##

### 동적 VM 관리 ###
- 사용자가 늘어나면 VM 생성
- 사용자가 줄어들면 VM 제거
- 끊김 없는 API 요청(사용자의 내부적 VM 위치가 달라져도 지속적인 서비스)

## UI 구성 ##

### 로그인 화면 <초기 화면> ###

### 회원가입 화면 ###

- ID/PW/Phone/OTP

### 대시보드 <Tabbar 1> ###

- 트레이더 오더 리스트
- 거래소 리스트

### 타겟 <Tabbar 2> ###

- 현재 사용자가 타겟을 코인별 정보가 나타남
- 타겟을 정한 코인별로 현재 거래 상황을 나타냄(매수상황/매도상황/손절상황)
- on/off/add/delete 로 타겟 설정이 가능함

#### 암호화폐 리스트 화면 ####

#### 암호화폐별 설정 화면 ####

### 거래 내역 <Tabbar 3> ###

- 매도/매수로 인한 거래내역이 실시간으로 나타남

### 내정보 <Tabbar 4> ###

- 거래소 별 API/로그인 설정
- 전체 계좌 금액 조회

#### 거래소 API 등록 화면 ####

- 거래소 별로 API 있으면 access_key, private_key 를 입력받고, API가 제공되지 않는 거래소는 ID/PW를 입력받는다.

## 기능 ##

### 매수 ###
- 매수 범위를 정할 수 있음
- 매수 가격은 매도 범위와 겹치지 않아야 하고 손절 트리거 보다 높아야 함

### 매도 ###
- 매도 범위를 정할 수 있음
- 매도 가격은 매수 범위와 겹치지 않아야 하고 손절 트리고 보다 높아야 함

### Stop loss(손절) ###

- 손절 트리거(손절이 시작되는 트리거)
- Full stop loss
- Partial stop loss
- Trailing stop loss

### 매집 ###

### 결제 시스템 ###

- 암호화폐 이용
- 월별 결제? 일별 결제?
- ETH 지갑

## API ##

### Upbit ###

- 계좌 정보
- Range 매도
- 매도 현황
- Range 매수
- 매수 현황
- 손절

## 상품 구성 ##

### 서버 선정 ###

- GCP 도쿄

![GCP-tokyo](./images/GCP-tokyo.png)

- AWS 서울

![AWS-seoul](./images/AWS-seoul-t2micro.png)

- AWS 도쿄

![AWS-tokyo](./images/AWS-tokyo-t2micro.png)

| 클라우드 | 리전  | TTL | Latency |
|--------|-------|-----|---------|
| GCP    | Tokyo |  58 | 1.5ms   |
| AWS    | Seoul |  51 | 2.47ms  |
| AWS    | Tokyo |  45 | 3.24ms  |

GCP는 계정당 무료 크래딧 $300을 받을 수 있고, 신용카드 하나로 5개의 계정 생성이 가능함.

### 성능별 구성 ###

| 상품     | 가격     | 코어수 | RAM | 점유 사용자 수 | API 사용 가능 개수 |
|----------|----------|--------|-----|----------------|--------------------|
| 기본형   | 20만/월  | 8      | 16  | 16             | 1/인당             |
| 고급형   | 35만/월  | 8      | 16  | 8              | 2/인당             |
| 프리미엄 | 80만/월  | 8      | 16  | 2              | 8/인당             |
| 마스터   | 120만/월 | 8      | 16  | 1              | 16/인당            |
