#+TITLE: Validation

* resources

** exchanges

| name   | type   | length | restrict  |
|--------+--------+--------+-----------|
| symbol | string | 3~20   | uppercase |
| name   | string | 3~100  | -         |

** markets/coins

| name   | type   | length | restrict  |
|--------+--------+--------+-----------|
| symbol | string | 3~10   | uppercase |
| name   | string | 3~50   | -         |
