#+TITLE: 데이터구조

* Version 1

** Order
#+BEGIN_SRC js
{
  "version": "string",
  "id": "string",
  "userId": "string",
  "title": "string",
  "desc": "string",
  "sourceId": "string",
  "exchange": "string",
  "market": "string",
  "coin": "string",
  "created_at": "datetime",
  "updated_at": "datetime",
  "finished_at": "datetime",
  "state": "string", // active | inactive | done
  "status": "string", // stuck | running | error
  "running_state": "string", // stoploss | profix
  "entry": {
    "quantity": "number",
    "strategy": [
      {
        "tradingIds": ["object"],
        "price": "number",
        "ratio": "number"
      }
    ]
  },
  "target": {
    "strategy": [
      {
        "tradingIds": ["object"],
        "price": "number",
        "ratio": "number"
      }
    ]
  },
  "stoploss": {
    "type": "number", // 0: 홀딩 / 1: stop-limit / 2: trailing-stop
    "strategy(stop-limit)": { // type에 따라 달라짐
      "tradingIds": ["object"],
      "ratio": "number",
      "stop": "number",
      "limit": "number"
    },
    "strategy(trailing-stop)": { // type에 따라 달라짐
      "tradingIds": ["object"],
      "ratio": "number",
      "stopRatio": "number"
    }
  }
}
#+END_SRC

** Trading
#+BEGIN_SRC js
{
  "version": "string",
  "id": "number",
  "orderId": "string",
  "userId": "string",
  "state": "string", // wait | cancel | done
  "side": "string", // bid | ask
  "created_at": "datetime",
  "checked_at": "datetime", // 마지막으로 체크된 시간
  "updated_at": "datetime", // 마지막으로 데이터 업데이트
  "exchange": "string",
  "market": "string",
  "coin": "string",
  "info": { // exchange에 따라 다름
  },
  "fee": {
    "executed": "number",
    "total": "number"
  },
  "volume": {
    "executed": "number",
    "total": "number"
  },
  "price": {
    "asking": "number", // 선택한 가격
    "executed": "number", // 체결된 가격
    "locked": "number"
  }
}
#+END_SRC
