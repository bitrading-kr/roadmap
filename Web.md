# Web

## UI

### /

- [ ] main : 메인페이지
- [ ] login : 로그인 페이지
- [ ] signin : 회원가입
- [ ] signin/recovery : ID/PW 찾기
- [ ] dashboard : 대시보드
- [ ] trading : 거래 내역
- [ ] trading/view/:tradingId : 거래 정보
- [ ] order : 오더 리스트
- [ ] order/add : 오더 추가
- [ ] order/edit/:orderId : 오더 수정
- [ ] order/view/:orderId : 오더 정보
- [ ] trader/recommend : 트레이더 추천
- [ ] trader/list : 트레이더 리스트
- [ ] trader/view/:traderId : 트레이더 상세 정보
- [ ] settings/api : API 설정
- [ ] settings/info : 사용자 정보

#### 연결

- [login](#login)
- [signin](#signin)

### login

사이트 홍보, 기능 소개, 매뉴얼

- [signin/recovery](#signinrecovery)
- [dashboard](#dashboard) : 로그인 완료시

### signin

회원가입

- [signin/recovery](#signinrecovery)

### signin/recovery

ID/PW 찾기

### dashboard

#### trading 카드

- [trading](#trading) : 카드 선택시
- [trading/view/:tradingId](#tradingviewtradingid) : 거래 ID 선택시
- [trader/view/:traderId](#traderviewtraderid) : 트레이더 ID 선택시

#### api status 카드

#### api sign 카드

- [settings/api](#settingsapi) : 수정 버튼 클릭시

#### order list 카드

- [order](#order): 카드 선택시
- [order/add](#orderadd) : 추가 버튼 클릭시
- [order/view/:orderId](#ordervieworderiId) : 아이템 클릭시

on/off 기능(비동기적 동작)

### trading

- [trading/view/:tradingId](#tradingviewtradingid) : 거래 ID 선택시
- [trader/view/:traderId](#traderviewtraderid) : 트레이더 ID 선택시

### trading/view/:tradingId

정보
- tradingId(UUID)
- trading status : 매도/매수 - wait/done / cancel
- trigger order : orderId
- market
- coin
- quantity

버튼
- 거래 취소
- 동일 거래 추가 : 같은 정보로 거래를 추가한다.

- [trader/view/:traderId](#traderviewtraderid) : 트레이더 ID 선택시

### order

주문 정보
- 

버튼
- 주문 추가 : 사용자 정의 주문을 추가한다

- [order/add](#orderadd) : 사용자 정의 주문을 추가한다
- [order/view/:orderId](#ordervieworderid) : 주문 아이템 클릭시
- [trader/view/:traderId](#traderviewtraderid) : 트레이더 ID 선택시

### order/add

사용자/트레이더 추가

### order/edit/:orderId

버튼
- 확인

### order/view/:orderId

버튼
- 수정 : 사용자 정의 주문만 수정 가능

- [order/edit/:orderId](#ordereditorderid) : 오더 ID 선택시
- [trader/view/:traderId](#traderviewtraderid) : 트레이더 ID 선택시

### trader/recommend

### trader/list

### trader/view/:traderId

### settings/api

### settings/info
